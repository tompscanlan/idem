# Import Python libs
import subprocess
import sys


def test_sls_shell(tree, runpy):
    cmd = f"{sys.executable} {runpy} state simple --sls-sources file://{tree}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout


def test_sls_implied_source(runpy, code_dir):
    sls = code_dir.joinpath("tests", "sls", "simple.sls")
    cmd = f"{sys.executable} {runpy} state {sls}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout


def test_sls_tree(runpy, tree):
    cmd = f"{sys.executable} {runpy} state simple --tree {tree}"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
