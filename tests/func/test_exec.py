# Import Python libs
import subprocess
import sys
import asyncio

# Import pop libs
import pop.hub


def run_ex(path, args, kwargs, acct_file=None, acct_key=None):
    """
    Pass in an sls list and run it!
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.states.test.ACCT = ["test_acct"]
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(
        hub.idem.ex.run(path, args, kwargs, acct_file, acct_key)
    )


def test_shell_exec(runpy):
    cmd = f"{sys.executable} {runpy} exec test.ping"
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    assert b"True" in ret.stdout


def test_exec(code_dir):
    acct_fn = code_dir.joinpath("tests", "files", "acct.fernet")
    acct_key = "eWO2UroAYY3Dff8uKcT32iiBHW2qVkVaDV3vIQoIaJU="
    ret = run_ex("test.ctx", [], {}, acct_file=acct_fn, acct_key=acct_key)
    assert ret == {"acct": {"foo": "bar"}}
